# Boekenbalie - InactiveModel

## The year is 2004
DHH is in the process of inventing Ruby-on-Rails.
So far only ActiveSupport has been created (magically it is already at version 7).
Next up is an ORM (Object-Relational Mapper). It might one day be called ActiveModel.

An intern has created a mock ORM so we can discuss what ActiveModel should look like.
The mockup will be thrown away later so we are calling it InactiveModel.  
For now, only integers and strings will be supported.
And only the embeddded SQLite database will be supported.

You have been tasked to review the code produced by the intern,
since you have a pretty good idea what the ORM should look like.
It is as if you already have seen what the finished thing would look like 😜

## Install
Code should work with Ruby 2.7 or newer.

Clone the repository and install:

    git clone git@bitbucket.org:ivo-edwhs/boekenbalie_inactive_model.git
    cd boekenbalie_inactive_model
    ruby -v # => Should return 'ruby 3.1.1' if you are using a ruby version manager
    bundle

Confirm that all `rspec` tests pass:

    rspec

Which should result in something like this:

```
Message
  sql
    can load all messages using raw sql
  inactive_model
    can instantiate a new message
    can determine fieldnames and -types
    can count all records
    can return all records
    can find a record by id
    can find a record by txt
    can raise an error when searching on a non-existent fieldname
    can create a new record
    can update a record
    can delete a record
    can add getters and setters for all fields

12 examples, 0 failures
```

## The problem
In source-file: `app/models/inactive_mode.rb`:

- please describe as much problems in InactiveModel as you can find
- please implement some quick fixes to InactiveModel, all rspec tests should still pass
- without implementing: describe how would you improve InactiveModel
- without implementing: are there any tests missing from message_spec ?
- is there anything positive to say about the code?
  We don't want our poor intern to change careers over this!

Please don't spend more than one or two hours on this case.
If you need more time you are probably overthinking it 🤔

## Copyright
All code owned by Boekenbalie B.V., The Netherlands, 2022
