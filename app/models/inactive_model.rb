# A base class
class InactiveModel

  attr_accessor :attributes, :saved

  # Instance methods
  def initialize(attributes, saved = false)
    @attributes = attributes
    @saved = saved
    add_getters_and_setters
  end

  def add_getters_and_setters
    self.attributes.keys.each do |f|
      define_singleton_method("#{f}")  { @attributes[f] }
      define_singleton_method("#{f}=") { |newval| @attributes[f] = newval }
    end
  end

  def save
    if @saved
      update(@attributes)
    else
      DB.execute(
        "INSERT INTO #{self.class.name.downcase + 's'}(#{attributes.keys.join(',') }) VALUES (#{attributes.map { |k, v| v.is_a?(Integer) ? v : "'#{v}'" }.join(',') })"
      )

      @saved = true
    end

    self
  end

  def update(attributes)
    attributes.each do |k, v|
      next if k == :id
      @attributes[k] = v
    end

    DB.execute(
      "UPDATE #{self.class.name.downcase + 's'} SET #{@attributes.select { |k, v| k != :id }.map { |k, v| v.is_a?(Integer) ? "#{k}=#{v}" : "#{k}='#{v}'" }.join(',') } WHERE id = #{@attributes[:id]}"
    )

    self
  end

  def delete
    if @saved && @attributes[:id].present?
      DB.execute("DELETE FROM #{self.class.name.downcase + 's'} WHERE id = #{@attributes[:id]}")
    end

    nil
  end

  # Class methods

  def self.attributes
    @attributes ||= DB.table_info(self.name.downcase + 's').map { |f| {
      name:  f['name'],
      type:  f['type'].split('(').first,
      limit: f['type'] =~ /varchar/ ?
        (f['type'].split('(').last.to_i) :
        2 ** (8 * 8 - 1),
    }
    }
  end

  def self.count
    self.all.length
  end

  def self.all
    DB.execute("SELECT * FROM #{self.name.downcase + 's'}")
  end

  def self.find(id)
    self.to_model(DB.execute("SELECT * FROM #{self.name.downcase + 's'} WHERE id = #{id} LIMIT 1").first)
  end

  def self.create(attributes)
    DB.execute("INSERT INTO #{self.name.downcase + 's'}(#{attributes.keys.join(',') }) VALUES (#{attributes.map { |k, v| v.is_a?(Integer) ? v : "'#{v}'" }.join(',') })")
    # Convert to model
    self.to_model(attributes)
  end

  def self.to_model(record)
    return unless record.present?

    ats = self.attributes.map.with_index do |a, i|
      [
        a[:name].to_sym,
        record[i]
      ]
    end.to_h

    eval("#{self.name}").new(ats, true)
  end

  def self.method_missing(m, *a, &b)
    finder = m.match(/\Afind_by_(.*)$/)

    if finder && !a.blank?
      return DB.execute("SELECT * FROM #{self.name.downcase + 's'} WHERE #{finder[1]} = '#{a.join('')}' LIMIT 1").first
    end

    nil
  end
end
