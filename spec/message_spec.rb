RSpec.describe Message do

  before(:context) do
    # Create a table
    DB.execute <<-SQL
      CREATE TABLE messages (
        id  int,
        txt varchar(42)
      );
    SQL

    # Create some records
    DB.execute 'INSERT INTO messages(id, txt) VALUES (1, "Hello world!")'
    DB.execute 'INSERT INTO messages(id, txt) VALUES (2, "Goodbye world!")'
  end

  after(:context) do
    # Cleanup
    if File.exist?('db/test.db')
      File.delete('db/test.db')
    end
  end

  describe 'sql' do
    it 'can load all messages using raw sql' do
      rows = DB.execute2('SELECT * FROM messages')

      expect(rows.length).to   eq 3

      expect(rows[0][0]).to eq 'id'
      expect(rows[0][1]).to eq 'txt'

      expect(rows[1][0]).to eq 1
      expect(rows[1][1]).to eq 'Hello world!'

      expect(rows[2][0]).to  eq 2
      expect(rows[2][1]).to  eq 'Goodbye world!'
    end
  end

  describe 'inactive_model' do
    it 'can instantiate a new message' do
      expect(Message.new({}).is_a?(Message)).to eq true
    end

    it 'can determine fieldnames and -types' do
      attributes = Message.attributes

      expect(attributes[0][:name]).to  eq 'id'
      expect(attributes[0][:type]).to  eq 'int'
      expect(attributes[0][:limit]).to be > 1e18

      expect(attributes[1][:name]).to  eq 'txt'
      expect(attributes[1][:type]).to  eq 'varchar'
      expect(attributes[1][:limit]).to eq 42
    end

    it 'can count all records' do
      expect(Message.count).to eq 2
    end

    it 'can return all records' do
      messages = Message.all
      expect(messages).to        be_a(Array)
      expect(messages.length).to eq 2
    end

    it 'can find a record by id' do
      expect(Message.find(1)).to be_present
    end

    it 'can find a record by txt' do
      expect(Message.find_by_txt('Hello world!')).to be_present
    end

    it 'can raise an error when searching on a non-existent fieldname' do
      expect {
        Message.find_by_status('A')
      }.to raise_error(SQLite3::SQLException)
    end

    it 'can create a new record' do
      Message.create(id: 3, txt: 'Three')
      expect(DB.execute('SELECT * FROM messages WHERE id=3').length).to eq 1
    end

    it 'can update a record' do
      Message.find(1).update(txt: 'New text')
      expect(DB.execute('SELECT * FROM messages WHERE txt="New text"').length).to eq 1
    end

    it 'can delete a record' do
      Message.find(1).delete
      expect(DB.execute('SELECT * FROM messages WHERE id=1').length).to eq 0
    end

    it 'can add getters and setters for all fields' do
      message = Message.find(2)
      expect(message.id).to  eq 2
      expect(message.txt).to eq 'Goodbye world!'
      message.txt = 'New'
      expect(message.txt).to eq 'New'
    end
  end
end
